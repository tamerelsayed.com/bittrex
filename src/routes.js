import React from 'react';
import { Switch, Route } from 'react-router-dom'

import App from './components/App';
import Markets from './components/Markets';
import NotFound from './components/NotFound';

import './assets/stylesheets/styles.css'

//<Route path='/schedule' component={Schedule}/>

const Routes = (props) => (
    <main>
        <Switch>
            <Route exact path='/' component={App}/>
            <Route path='/market/:Market' component={ Markets } />
            <Route path='*' component={NotFound}/>
        </Switch>
    </main>
);

export default Routes;