import React from 'react';
import { render }  from 'react-dom';
import DevTools from "mobx-react-devtools";

import App from './components/App';

import './assets/stylesheets/styles.css'
// import { BrowserRouter, Route } from 'react-router-dom';

// import Routes from './routes';

render(
    <div>
        <DevTools />
        <App />
    </div>,
    document.getElementById('root')
);