import React, { Component } from 'react'
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'


@observer
class JsxList extends Component {

    render() {
        const { thisListObj, thisListName, thisListOnClick } = this.props

        return (
            <div>
                { Object.keys( thisListObj ).map( (key, index) =>
                    {
                        return (
                            <div className="cardBed" key={`Card-${key}`}>
                                <Card>
                                    <CardHeader
                                        title={ key }
                                        titleColor="rgb(33, 150, 243)"
                                        actAsExpander={true}
                                        showExpandableButton={true}
                                    />
                                    <CardText>
                                        <ul className="flex-container" >
                                            <li className="flex-item"><div className="flex-inner">{ `Last: ${thisListObj[key].Last}` }</div></li>
                                            <li className="flex-item"><div className="flex-inner">{ `24H High: ${thisListObj[key].High}` }</div></li>
                                            <li className="flex-item"><div className="flex-inner">{ `24H Low: ${thisListObj[key].Low}` }</div></li>
                                            <li className="flex-item"><div className="flex-inner">{ `Volume: ${thisListObj[key].Volume}` }</div></li>
                                        </ul>
                                    </CardText>
                                </Card>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

export default JsxList;