import React, { Component } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'

import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Checkbox from 'material-ui/Checkbox';

const appState = observable({
    thisListObj : [],
    currentWatchList: [],
    thisListName: '',
    Bttnstyle: {
        display: "block"
    },
    btnDivStyle: {
        padding: "5px"
    },
    listStyle :
    {
        display: "none"
    }
})

@observer
class MakeList extends Component
{
    render() {
        return (
            <ListItem
                primaryText={ this.props.market.name }
                leftCheckbox={
                    <Checkbox
                        checked={ this.props.market.checked }
                        id={ this.props.market.name }
                        name={ this.props.id.toString() }
                        onCheck={ this.props.clickCheckBox }
                    />
                }
            />
        )
    }
}

@observer
class WatchList extends Component {

    constructor( props )
    {
        super(props);

        this._clickCheckBox = this._clickCheckBox.bind(this);
    }

    componentWillUpdate( nextProps, nextState )
    {
        if ( nextProps.appState !== "Watchlist" && this.props.appState !== nextProps.appState )
        {

            appState.listStyle = { display: "none" }
        }
        else if ( this.props.appState !== nextProps.appState )
        {
            appState.listStyle = { display: "block" }
        }
    }

    _clickCheckBox( event, checked )
    {
        const target = event.target
        const KeyNr = parseInt( target.name )
        const MarketName = appState.thisListObj[KeyNr].name
        let newCurrentWatchList = []

        appState.thisListObj[KeyNr].checked = checked

        if ( checked )
        {
            appState.currentWatchList.push( MarketName )
        }
        else
        {
            appState.currentWatchList.map(function ( MarketN ) {
                if ( MarketName !== MarketN && MarketN !== null && typeof MarketN !== 'undefined')
                {
                    newCurrentWatchList.push( MarketN )
                }
            })
            appState.currentWatchList = newCurrentWatchList
        }

        this.props.returnWatchList( appState.currentWatchList )
    }

    _clickMarketType( market )
    {
        const { listObj, currentWatchList } = this.props
        let thisListObj = []
        const btnDivStyle = {
            display: "none"
        }

        Object.keys( listObj[market] ).map( (key, index) => {
            let checked = false
            if ( currentWatchList.indexOf( key ) !== -1 )
            {
                checked = true
            }

            thisListObj.push( { name: key, checked: checked  })
        })

        appState.thisListObj = thisListObj
        appState.thisListName = market
        appState.btnDivStyle = btnDivStyle
        appState.currentWatchList = currentWatchList
    }


     render() {

        return(
            <div style={ appState.listStyle } >
                <div style={ appState.btnDivStyle }>
                    <RaisedButton
                        label="BTC"
                        fullWidth={true}
                        onClick={ () => this._clickMarketType( "BTC" ) }
                        style={ appState.Bttnstyle }
                    />
                </div>
                <div style={ appState.btnDivStyle }>
                    <RaisedButton
                        label="ETH"
                        fullWidth={true}
                        onClick={ () => this._clickMarketType( "ETH" ) }
                        style={ appState.Bttnstyle }
                    />
                </div>
                <div style={ appState.btnDivStyle }>
                    <RaisedButton
                        label="USDT"
                        fullWidth={true}
                        onClick={ () => this._clickMarketType( "USDT" ) }
                        style={ appState.Bttnstyle }
                    />
                </div>
                <List>
                    <Subheader>{ appState.thisListName }</Subheader>
                    {
                        appState.thisListObj.map( ( data, i) =>{
                            return (
                                <MakeList
                                    market={ data }
                                    key={i}
                                    id={i}
                                    clickCheckBox={ this._clickCheckBox }
                                />
                            )
                        })
                    }
                </List>

            </div>
        )
    }
}

export default WatchList