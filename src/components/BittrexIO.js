import io from 'socket.io-client';
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'
import autobind from 'autobind-decorator'

@autobind
class BittrexIO {

    @observable MarketNames = {};
    @observable MarketSummaries = [];
    @observable currentWatchList = [];
    @observable updateExchangeState = [];
    @observable updateSummaryState = [];
    @observable watchListMI = {};
    @observable watchListMIloading = true;

    constructor( ) {
        this.socket = io('http://localhost:3000', {reconnect: true});

        this.setupSocket();
        this.onStart();
    }

    @action
    setupSocket() {
        this.socket.on('dong', () => {
            this.latency = Date.now() - this.startPingTime;
            console.log( 'IO >> Ping: ' + this.latency + 'ms' )
        });

        this.socket.on('connect_failed', () => {
            console.log( 'IO >> connect_failed' )
            this.socket.close();
        });

        this.socket.on('disconnect', () => {
            console.log( 'IO >> disconnect' )
            this.socket.close();
        });

        this.socket.on('connected', () => {
            console.log( 'IO >> connected' )
            this.checkLatency()
        });

        this.socket.on('EMIT_MarketNames', ( from, MarketNames ) => {
            console.log( "MarketNames", MarketNames )
            console.log( "this.MarketNames", this.MarketNames )
            this.MarketNames = MarketNames
        });

        this.socket.on('EMIT_watchList', ( from, data ) => {
            console.log( 'EMIT_watchList', data )
            if ( typeof data.Currencies !== 'undefined' )
            {
                this.currentWatchList = data.Currencies
                console.log( typeof this.MarketSummaries.BTC )
                console.log( this.MarketSummaries )
                if ( typeof this.MarketSummaries.BTC !== 'undefined' && this.MarketSummaries.BTC.slice() !== [] )
                {
                    this.MakeMarketInfo('makeWatchListMI', this.MarketSummaries )
                }
            }
        });

        this.socket.on('EMIT_updateExchangeState', ( from, updateExchangeState ) => {
            this.updateExchangeState = updateExchangeState

            this.MakeMarketInfo('updateExchangeState', updateExchangeState )
        });

        this.socket.on('EMIT_updateSummaryState', ( from, updateSummaryState ) => {
            console.log( 'EMIT_updateSummaryState >> updateSummaryState', updateSummaryState )
            console.log( 'EMIT_updateSummaryState >> typeof updateSummaryState', typeof updateSummaryState )
            this.updateSummaryState = updateSummaryState
        });

        this.socket.on('EMIT_MarketSummaries', ( from, MarketSummaries ) =>
        {
            console.log( 'EMIT_MarketSummaries', MarketSummaries )
            if ( typeof MarketSummaries !== 'undefined' )
            {
                this.MarketSummaries = MarketSummaries
            }
        });
    }

    @action
    MakeMarketInfo( type, Data )
    {
        const currentWatchList = this.currentWatchList.slice()

        let {
            watchListMI,
            MarketNames,
            marketInfo
        } = this

        if ( type === 'MarketNames' )
        {
            Object.keys(Data).map( (key, index) => {
                Data[key].forEach( ( data_for ) => {

                    if ( typeof MarketNames[ key ] === "undefined" )
                    {
                        MarketNames[ key ] = { }
                    }

                    MarketNames[ key ][ data_for ] = { }
                })
            });

            this.MarketNames = MarketNames
        }
        else if ( type === 'makeWatchListMI'  )
        {
            Data.BTC.forEach( ( data_for ) => {
                const MarketName = data_for.MarketName

                if ( currentWatchList.indexOf( MarketName ) !== -1 )
                {
                    watchListMI[ MarketName ] = data_for
                }
            })
            this.watchListMIloading = false;
            this.watchListMI = watchListMI
        }
        else if ( type === 'updateExchangeState'  )
        {

            const MarketName = Data.MarketName
            if ( currentWatchList.indexOf( MarketName ) !== -1 )
            {
                watchListMI[ MarketName ].Buys = Data.Buys
                watchListMI[ MarketName ].Fills = Data.Fills
                watchListMI[ MarketName ].Sells = Data.Sells
            }

            this.watchListMI = watchListMI
        }
        else if ( type === 'updateSummaryState'  )
        {
            let currentWatchListCount = currentWatchList.length;
            Data.forEach( ( data_for ) => {
                const MarketName = data_for.MarketName
                if ( currentWatchList.indexOf( MarketName ) !== -1 )
                {
                    watchListMI[ MarketName ] = data_for
                }
            })

            this.watchListMI = watchListMI
        }
    }

    checkLatency()
    {
        this.startPingTime = Date.now();
        this.socket.emit( 'ding' );
    }

    onStart()
    {
        this.socket.emit('GetWatchList', 'BittrexReactClient', {
            query: { user: 'Phara0h' },
            user: 'Phara0h'
        })

        this.socket.emit('GET_MarketNames', 'BittrexReactClient')
    }
}

export default new BittrexIO