import React, { Component } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'

import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Checkbox from 'material-ui/Checkbox';

import JsxList from './JsxList';



@observer
class HomeWatchList extends Component {

    @observable appState = {
        styleCss : { display: "block" },
        ListName: '',
        thisListObj: { }
    }

    constructor(props) {
        super(props);
    }

    @action
    componentWillUpdate( nextProps, nextState )
    {
        console.log( " componentWillUpdate >> nextProps.listObj", nextProps.listObj )
        console.log( " componentWillUpdate >> this.props.listObj", this.props.listObj )
        if ( nextProps.appState !== "Home" && this.props.appState !== nextProps.appState )
        {
            this.appState.styleCss = { display: "none" }
        }
        else if ( this.props.appState !== nextProps.appState )
        {
            this.appState.styleCss = { display: "block" }
        }

        this.appState.thisListObj = nextProps.listObj
    }

    _clickListItem()
    {
        console.log( " Click")
    }

    render() {

        const thisListName = "BTC"

        if ( this.props.loading )
        {
            return(
                <div id="preloader" style={ this.appState.styleCss } >
                    <div className="portrait">
                        <div className="loader"></div>
                    </div>
                    <div className="lTXT">loading</div>
                </div>
            )
        }
        else
        {
            console.log(" RENDER >>", this.props.thisListObj )
            return (
                <div style={ this.appState.styleCss } >

                    <JsxList
                        thisListObj={ this.props.thisListObj }
                        thisListName={ thisListName }
                        thisListOnClick={ this._clickListItem }
                    />

                </div>
            )
        }
    }
}

export default HomeWatchList;
