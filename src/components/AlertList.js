import React, { Component } from 'react'

import RaisedButton from 'material-ui/RaisedButton';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Checkbox from 'material-ui/Checkbox';


export default class AlertList extends Component {

    constructor( props )
    {
        super(props);
        this.state = {
            thisListObj : [],
            thisListName: '',
            Bttnstyle: {
                display: "block"
            },
            btnDivStyle: {
                padding: "5px"
            },
            listStyle :
            {
                display: "none"
            }
        };

        this.makeList = this.makeList.bind(this);
    }

    makeList( market, i )
    {
        return(
            <ListItem key={`wCB-list${market.name}`} primaryText={ market.name } leftCheckbox={ <Checkbox checked={ market.checked } id={ market.name } name={ i.toString() } onCheck={ this.clickCheckBox } /> } />
        )
    }

    render() {

        const { thisListObj, thisListName, Bttnstyle, btnDivStyle, listStyle } = this.state
        const { styleCss, returnWatchList } = this.props

        return(
            <div style={ styleCss } >

                <List>
                    <Subheader>{ thisListName }</Subheader>
                    {
                        thisListObj.map( ( data, i) =>{
                            return this.makeList( data, i  )
                        })
                    }
                </List>
            </div>
        )
    }
}
