import React, { Component } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'

import AppBar from 'material-ui/AppBar'
import ContentAdd from 'material-ui/svg-icons/content/add';
import { ActionHome, EditorFormatListBulleted, DeviceAccessAlarm, ContentInbox } from 'material-ui/svg-icons/';
import Drawer from 'material-ui/Drawer'
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import { List, ListItem, makeSelectable } from 'material-ui/List'
import NavigationClose from 'material-ui/svg-icons/navigation/close';

import JsxList from './JsxList'

const appState = observable({
    drawOpen: false,
    active : {
        Home : {
            active : true,
            title : 'Bittrex IO'
        },
        Watchlist : {
            active : false,
            title : 'Watchlist'
        },
        Alerts : {
            active : false,
            title : 'Alert List'
        },
        Orders : {
            active : false,
            title : 'Order List'
        }
    },
    AppBarTitle : 'Bittrex IO'
});

@observer
class HeaderDrawer extends Component
{
    constructor( props )
    {
        super( props );
    }

    componentWillUpdate( nextProps, nextState )
    {
        if ( this.props.appState !== nextProps.appState )
        {
            this._changeAppState( nextProps.appState )
        }
    }

    render()
    {
        return (
            <div>
                <AppBar
                    title={ appState.AppBarTitle }
                    onLeftIconButtonTouchTap={ this._toggleDraw }
                />
                <Drawer
                    open={ appState.drawOpen }
                    docked={ false }
                    onRequestChange={ this._toggleDraw }
                >
                    <List>
                        <ListItem
                            disabled={ appState.active.Home.active }
                            primaryText="Home"
                            id="Home"
                            onClick={ () => this._clickListItem( 'Home' ) }
                            leftIcon={<ActionHome />}
                        />
                        <ListItem
                            disabled={ appState.active.Watchlist.active }
                            primaryText="Watchlist"
                            id="Watchlist"
                            onClick={ () => this._clickListItem( 'Watchlist' ) }
                            leftIcon={<EditorFormatListBulleted />}
                        />
                        <ListItem
                            disabled={ appState.active.Alerts.active }
                            primaryText="Alerts"
                            id="Alerts"
                            onClick={ () => this._clickListItem( 'Alerts' ) }
                            leftIcon={<DeviceAccessAlarm />}
                        />
                        <ListItem
                            disabled={ appState.active.Orders.active }
                            primaryText="Open Orders"
                            id="Orders"
                            onClick={ () => this._clickListItem( 'Orders' ) }
                            leftIcon={<ContentInbox />}
                        />
                    </List>
                </Drawer>
            </div>
        )
    }

    _clickListItem( event )
    {
        console.log( event )
        this._changeAppState( event )
        this.props.changeAppState( event )
        this._toggleDraw()
    }

    _changeAppState( event )
    {
        let {
            AppBarTitle,
            active
        } = appState

        Object.keys(active).map(function(key, index) {
            active[key].active = false;
            if ( key === event )
            {
                active[key].active = true;
                AppBarTitle = active[key].title
            }
        });

        appState.AppBarTitle = AppBarTitle
        appState.active = active
    }

    _toggleDraw()
    {
        appState.drawOpen = !appState.drawOpen
    }
}

export default HeaderDrawer;