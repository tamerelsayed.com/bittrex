import React, { Component } from 'react'
import BittrexIO from './BittrexIO'
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import FlatButton from 'material-ui/FlatButton';
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card';

export default class App extends Component {

    constructor( props ) {
        super(props);
        this.state = {
            MarketNames: {},
            updateExchangeState: {},
            updateSummaryState: {},
            marketInfo: {
                BTC : {},
                ETH : {},
                USDT : {}
            }
        };
    }

    componentWillMount() {

        const BittrexIOfunc = new BittrexIO()

        BittrexIOfunc.socket.emit('GET_MarketNames')

        BittrexIOfunc.socket.on('EMIT_MarketNames', ( from, MarketNames ) => {
            this.MakeMarketInfo('MarketNames', MarketNames )
        });

        BittrexIOfunc.socket.on('EMIT_updateExchangeState', ( from, updateExchangeState ) => {
            this.MakeMarketInfo('updateExchangeState', updateExchangeState )
            // this.setState({
            //     updateExchangeState
            // });
        });

        BittrexIOfunc.socket.on('EMIT_updateSummaryState', ( from, updateSummaryState ) => {
            this.MakeMarketInfo('updateSummaryState', updateSummaryState )
            // this.setState({
            //     updateSummaryState
            // });
        });
    }

    MakeMarketInfo( type, Data )
    {

        let { marketInfo } = this.state
        if ( type === 'MarketNames' )
        {
            Object.keys(Data).map(function(key, index) {
                Data[key].forEach(function( data_for ) {
                    marketInfo[ key ][ data_for ] = { }
                })
            });

        }
        else if ( type === 'updateExchangeState'  )
        {
            const MarketName = Data.MarketName,
            keyName = MarketName.split('-')[0]
            Object.keys( Data ).map(function(key, index) {
                marketInfo[ keyName ][ MarketName ][key] = Data[key]
            })
        }
        else if ( type === 'updateSummaryState'  )
        {
            Data.forEach(function( data_for ) {
                const MarketName = data_for.MarketName,
                keyName = MarketName.split('-')[0]
                Object.keys( data_for ).map(function(key, index) {
                    marketInfo[ keyName ][ MarketName ][key] = data_for[key]
                })
            })
        }

        this.setState({
            marketInfo
        });

    }

    makeMarketCard(marketName, marketPrize, updateSummaryState)
    {
        const style = {

        }
        return(
            <Card showExpandableButton={ true }>
                <CardHeader
                    title={ marketName }
                    subtitle={ marketPrize }
                />

                <CardHeader
                    expandable={ true }
                    title={ marketName }
                    subtitle={ marketPrize }
                />

                <CardText expandable={ true }>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                  Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                  Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                </CardText>
                <CardActions expandable={ true } >
                  <FlatButton label="Action1"/>
                  <FlatButton label="Action2"/>
                </CardActions>
            </Card>
        )
    }

    render() {
        const {
            MarketNames,
            updateExchangeState,
            updateSummaryState
        } = this.state

        return(
            <MuiThemeProvider muiTheme={getMuiTheme()}>
                <CardText expandable={ true }>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Donec mattis pretium massa. Aliquam erat volutpat. Nulla facilisi.
                  Donec vulputate interdum sollicitudin. Nunc lacinia auctor quam sed pellentesque.
                  Aliquam dui mauris, mattis quis lacus id, pellentesque lobortis odio.
                </CardText>
            </MuiThemeProvider>
        )

    }
}