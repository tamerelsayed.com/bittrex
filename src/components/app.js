import React, { Component } from 'react'
import { observable, action } from 'mobx'
import { observer } from 'mobx-react'

import AppBar from 'material-ui/AppBar'
import ContentAdd from 'material-ui/svg-icons/content/add';
import Drawer from 'material-ui/Drawer'
import FlatButton from 'material-ui/FlatButton'
import FloatingActionButton from 'material-ui/FloatingActionButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import { List, ListItem } from 'material-ui/List'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import {Card, CardActions, CardHeader, CardText, CardTitle} from 'material-ui/Card'

import WatchList from './WatchList'
import HomeWatchList from './HomeWatchList'
import AlertList from './AlertList'
import HeaderDrawer from './HeaderDrawer'
import BittrexIO from './BittrexIO'



@observer
class FloatingActionBTN extends Component
{
    render()
    {
        if ( this.props.appState === 'Home' )
        {
            return(
                <FloatingActionButton
                    className="watchAddBTN"
                    onClick={ () => this.props.changeAppState('Watchlist') }
                >
                    <ContentAdd />
                </FloatingActionButton>
            )
        }
        return null
    }
}

@observer
class App extends Component {

    @observable appState = {
        appState : 'Home',
        currentWatchList : [],

        watchListMI : {},
        watchListMIloading: true,

        MarketSummaries: [],
        MarketNames : {},
        marketInfo : {},

        updateExchangeState : {},
        updateSummaryState : {}
    };

    componentWillUpdate( nextProps, nextState )
    {
        console.log( " nextState ", nextState )
        console.log( " nextProps ", nextProps )
    }


    @action
    _changeAppState( newState )
    {
        this.appState.appState = newState
    }

    @action
    _updateWatchlist( newList )
    {
        console.log( "_updateWatchlist newList", newList )
        BittrexIO.socket.emit('UpdateWatchList', 'BittrexReactClient', {
            query: { user: 'Phara0h' },
            Mutation: { Currencies: newList }
        })
    }

    @action
    _updateAlertlist( newList )
    {
         BittrexIO.socket.emit('UpdateAlertList', 'BittrexReactClient', {
            query: { user: 'Phara0h' },
            Mutation: { Alerts: newList }
        })
    }

    render() {
        console.log('APP render >>', BittrexIO.watchListMI )
        return(
            <MuiThemeProvider muiTheme={getMuiTheme()} >
                <div>
                    <HeaderDrawer
                        appState={ this.appState.appState }
                        changeAppState= { this._changeAppState }
                    />

                    <WatchList
                        appState={ this.appState.appState }
                        listObj={ this.appState.MarketNames }
                        returnWatchList={ this._updateWatchlist }
                        currentWatchList={ this.appState.currentWatchList }
                    />

                    <AlertList
                        appState={ this.appState.appState }
                        styleCss={ this.appState.alertListStyling }
                        listObj={ this.appState.AlertListArr }
                        returnWatchList={ this._updateAlertlist }
                        currentWatchList={ this.appState.currentAlertList }
                    />

                    <HomeWatchList
                        appState={ this.appState.appState }
                        BittrexIO={ BittrexIO.watchListMIloading }
                        listObj={ BittrexIO.watchListMI }
                    />

                    <FloatingActionBTN
                        appState={ this.appState.appState }
                        changeAppState={ this._changeAppState }
                    />

                </div>
            </MuiThemeProvider>
        )

    }
}

export default App;