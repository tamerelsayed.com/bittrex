const webpackDev = require('./webpack.dev.config.js'); // Dev
const webpackPro = require('./webpack.production.config.js'); // production

const NodeEnviorment =  typeof process.env.NODE_ENV === 'string' ? process.env.NODE_ENV.replace(' ', '' ) : ''
if ( NodeEnviorment === 'production' ) {
    console.log(' -=== production ===-')
    module.exports = webpackPro
}else {
    console.log(' -=== DEV ===-')
    module.exports = webpackDev
}