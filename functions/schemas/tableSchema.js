import mongoose, { Schema } from 'mongoose';

export const tableSchema = new Schema({
  TimeStamp: {
    type: String
  },

  TxFee: {
    type: Number
  },

  Last: {
    type: Number
  },

  Volume: {
    type: Number
  },

  BaseVolume: {
    type: Number
  },

  Bid: {
    type: Number
  },

  Ask: {
    type: Number
  },

  OpenBuyOrders: {
    type: Number
  },

  OpenSellOrders: {
    type: Number
  },

  Buys: {
    type: Schema.Types.Mixed
  },

  Sells: {
    type: Schema.Types.Mixed
  },

  Fills: {
    type: Schema.Types.Mixed
  }

}, {timestamps: true});

//export const LoggingDB = mongoose.model( 'Logging', LoggingSchema );