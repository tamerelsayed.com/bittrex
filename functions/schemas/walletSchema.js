import mongoose, { Schema } from 'mongoose';

export const walletSchema = new Schema({

  Currency: {
    type: String
  },

  Balance: {
    type: Number
  },

  Available: {
    type: Number
  },

  Pending:{
    type: Number
  },

  uuid: {
    type: String
  }
});

export const walletDB = mongoose.model( 'wallet', walletSchema );