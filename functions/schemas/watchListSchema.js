import mongoose, { Schema } from 'mongoose';

export const watchListSchema = new Schema({

  user: {
    type: String,
    unique: true
  },

  Currencies: {
    type: Schema.Types.Mixed
  },

  Alerts : {
    type: Schema.Types.Mixed
  },


  Pending:{
    type: Number
  },


});

export const watchListDB = mongoose.model( 'watchList', watchListSchema );