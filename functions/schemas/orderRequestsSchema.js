import mongoose, { Schema } from 'mongoose';


export const orderRequestSchema = new Schema({

  TimeStamp: {
    type: String
  },

  Market: {
    type: Number
  },

  Type: {
    type: String
  },

  OpenDate:{
    type: String
  },

  CloseDate: {
    type: String
  },

  Price: {
    type: Number
  },

  UnitsFilled: {
    type: Number
  },

  UnitsTotal: {
    type: Number
  },

  OpenBuyOrders: {
    type: Number
  },

  ActualRate:{
    type: Number
  },

  CostProceeds: {
    type: Number
  },

  uuid: {
    type: String,
    unique: true
  }

});

export const orderRequestDB = mongoose.model( 'orderRequest', orderRequestSchema );