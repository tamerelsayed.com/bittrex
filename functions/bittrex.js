import bittrex from 'node-bittrex-api';
import { updateSummaryStateDB, updateExchangeState } from './mongoDB';
import BittrexIO from './bittrexIO';

const BittrexIOsocket = new BittrexIO()

bittrex.options({
  'apikey' : 'dc847f9c09ea4a969903ed14d09b48ac',
  'apisecret' : '69303f236e9545e495179e0b2c775411',
  'verbose' : true,
});

// make this a cleint

export const startWebsocket = ( markets ) =>
{
    bittrex.websockets.subscribe( markets, function(data, client) {

        if (data.M === 'updateExchangeState') {
            data.A.forEach(function(data_for) {
                BittrexIOsocket.sendUpdateExchangeState( data_for )
            });
        }
    });
}

//Used to get all supported currencies at Bittrex along with other meta data.
const getCurrencies = ( ) =>
{
    return new Promise( ( resolve, reject ) => {
        bittrex.getcurrencies( function( data, err )
        {
          err ? reject(err) : resolve(data)
        });
    })
}

//Used to get the current tick values for a market.
const getTicker = ( marketName ) =>
{
    return new Promise( ( resolve, reject ) => {
        bittrex.getticker( { market : marketName }, function( data, err )
        {
          err ? reject(err) : resolve(data)
        });
    })
}

//Used to get the last 24 hour summary of all active exchanges
export const getMarketSummaries = ( MarketSummaries, MarketNames ) =>
{

    return new Promise( ( resolve, reject ) => {
        bittrex.getmarketsummaries( function( data, err )
        {
            if ( err )
            {
                reject(err)
            }
            else
            {
                data.result.forEach(function( data_for ) {
                    if ( data_for.MarketName.indexOf('BTC-') !== -1 )
                    {
                        MarketSummaries.BTC.push( data_for )
                        MarketNames.BTC.push( data_for.MarketName )
                    }
                    else if ( data_for.MarketName.indexOf('ETH-') !== -1 )
                    {
                        MarketSummaries.ETH.push( data_for )
                        MarketNames.ETH.push( data_for.MarketName )
                    }
                    else if ( data_for.MarketName.indexOf('USDT-') !== -1 )
                    {
                        MarketSummaries.USDT.push( data_for )
                        MarketNames.USDT.push( data_for.MarketName )
                    }
                })
                resolve({ MarketSummaries, MarketNames })
            }
        });
    })
}


//Used to get the last 24 hour summary of all active exchanges
const getMarketSummary = ( marketName ) =>
{
    return new Promise( ( resolve, reject ) => {
        bittrex.getmarketsummary( { market : marketName }, function( data, err )
        {
          err ? reject(err) : resolve(data)
        });
    })
}

//Used to retrieve the latest trades that have occured for a specific market.
const getMarketHistory = ( marketName ) =>
{
    return new Promise( ( resolve, reject ) => {
        bittrex.getmarkethistory( { market : marketName }, function( data, err )
        {
          err ? reject(err) : resolve(data)
        });
    })
}

// order book of a market
const getOrderbook = ( market ) =>
{
    return new Promise( ( resolve, reject ) => {
        bittrex.getorderbook({ market : market, depth : 10, type : 'both' }, function( data, err )
        {
          err ? reject(err) : resolve(data)
        });
    })
}

// get wallet balance for a
const getWalletbalance = ( market = false ) =>
{
    return new Promise( ( resolve, reject ) => {
        let specificMarket = '';
        if ( typeof market === 'string' )
        {
            specificMarket = '?currency=' + market;
        }

        bittrex.sendCustomRequest( 'https://bittrex.com/api/v1.1/account/getbalances'+specificMarket, function( data, err )
        {
            err ? reject(err) : resolve(data)

        }, true );
    })
}

// get wallet balance for a
const getOpenOrders = ( market = false ) =>
{
    return new Promise( ( resolve, reject ) => {
        let specificMarket = '';
        if ( typeof market === 'string' )
        {
            specificMarket = '?market=' + market;
        }

        bittrex.sendCustomRequest( 'https://bittrex.com/api/v1.1/account/getopenorders'+specificMarket, function( data, err )
        {
            err ? reject(err) : resolve(data)

        }, true );
    })
}


