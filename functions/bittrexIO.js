import io from 'socket.io-client';

export default class BittrexIO {

    constructor() {
        this.socket = io('http://localhost:3000', {reconnect: true});
        this.marketInfo = {
            BTC : {},
            ETH : {},
            USDT : {}
        }
        this.setupSocket();
    }

    setupSocket() {
        this.socket.on('dong', () => {
            this.latency = Date.now() - this.startPingTime;
            this.addSystemLine('Ping: ' + this.latency + 'ms');
        });

        this.socket.on('connect_failed', () => {
            this.socket.close();
        });

        this.socket.on('disconnect', () => {
            this.socket.close();
        });
    }


    checkLatency() {
        this.startPingTime = Date.now();
        this.socket.emit('ding');
    }

    sendUpdateExchangeState ( data )
    {
        this.socket.emit('updateExchangeState', 'BittrexIO', data );
        //parse data per market and emit
        //this.socket.emit('updateExchangeState', 'BittrexIO', data );
    }

    sendUpdateSummaryState ( data )
    {
        this.socket.emit('updateSummaryState', 'BittrexIO', data );
    }
}





