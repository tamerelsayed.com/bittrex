import mongoose, { Schema } from 'mongoose';
import assert from 'assert';
import { orderDB } from './schemas/orderSchema';
import { orderRequestDB } from './schemas/orderRequestsSchema';
import { tableSchema } from './schemas/tableSchema';
import { walletDB } from './schemas/walletSchema';
import { watchListDB } from './schemas/watchListSchema';

const COMPOSE_URI_DEFAULT = 'mongodb://localhost:27017/bittrex';

let currencyTables = {};

mongoose.Promise = require('bluebird');
mongoose.connect(COMPOSE_URI_DEFAULT, {
  useMongoClient: true
})
.then( ( db ) => {
  console.log('mongo connected')
  //console.log(db)
})
.catch( ( error ) => {
  console.error(error)
})

export const makeEntry = ( DB, input ) =>
{
  const newEntry = new DB ({
    ...input
  });

  newEntry.id = DB._id;
  return new Promise((resolve, reject) => {
    newEntry.save(function (err) {
      err ? reject(err) : resolve( newEntry )
    })
  })
}

const updateEntry = (DB, query2find, newData) =>
{
  return new Promise( ( resolve, reject ) =>
  {
    dbLookUp ( DB, query2find )
    .then( ( result ) => {
      DB.findByIdAndUpdate(result[0]._id, { $set: { ...newData }}, { new: true }, function (err, results) {
        err ? reject(err) : resolve(results)
      })

    })
    .catch( ( error ) =>
    {
      makeEntry( DB, newData )
      .then( ( result ) =>
      {
        resolve( result );
      })
      .catch( (makeEntryError) =>
      {
        reject( { dbLookUp: error, makeEntry: makeEntryError } );
      })

    })
  })
}

const dbLookUp = ( db, newQuery ) => {
  return new Promise( ( resolve, reject ) => {
    let QueryTest = db.find(
      newQuery,
      { score : { $meta: "textScore" } }
    )
    .sort({ score : { $meta : 'textScore' } })
    mongoose.Promise = require('bluebird');
    assert.equal(QueryTest.exec().constructor, require('bluebird'));
    // A query is not a fully-fledged promise, but it does have a `.then()`.
    QueryTest.then( (result) => {
      resolve(result);
    })
    .catch( (error) => {
      reject(error);
    })
  })
}

const dbFindAll = ( db ) =>
{
  return new Promise( (resolve, reject) => {
    db.find( (err, data) =>{
      err ? reject(err) : resolve(data)
    })
  });
}


export const updateSummaryStateDB = ( data ) =>
{
  data.forEach( function(data_for) {
// if ( data_for.MarketName.indexOf("BTC-") !== -1 ){

      const MarketName = data_for.MarketName
      if ( typeof currencyTables[MarketName] === 'undefined' )
      {
        currencyTables[MarketName] = mongoose.model( MarketName, tableSchema );
      }

      const DBdata = {
          TimeStamp : data_for.TimeStamp
        , Last : data_for.Last
        , BaseVolume : data_for.BaseVolume
        , Bid : data_for.Bid
        , Ask : data_for.Ask
        , OpenBuyOrders : data_for.OpenBuyOrders
        , OpenSellOrders : data_for.OpenSellOrders
      }

      makeEntry ( currencyTables[MarketName], DBdata )
      .then( ( result ) =>{
        //console.log( "Success DB entry ", result )
      })
      .catch( ( error ) => {
        //console.log( "ERROR DB entry ", error )
      })
    //}
  });
}

export const updateExchangeState = ( data ) =>
{
  const MarketName = data.MarketName
  console.log( "MarketName >> ",MarketName)
  if ( typeof currencyTables[MarketName] === 'undefined' )
  {
    currencyTables[MarketName] = mongoose.model( MarketName, tableSchema );
  }

  const DBdata = {
      Buys : data.Buys
    , Sells : data.Sells
    , Fills : data.Fills
  }

  makeEntry ( currencyTables[MarketName], DBdata )
  .then( ( result ) =>{
    //console.log( "Success DB entry ", result )
  })
  .catch( ( error ) => {
    //console.log( "ERROR DB entry ", error )
  })
}

export const getWatchlist = ( query ) =>
{
  return new Promise( ( resolve, reject ) => {
    dbLookUp( watchListDB, query )
    .then( ( result ) =>{
      resolve( result )
    })
    .catch( ( error ) => {
      reject( error )
    })
  })
}

export const saveWatchlist = ( query, data ) =>
{
  return new Promise( ( resolve, reject ) => {
    updateEntry( watchListDB, query, data )
    .then( ( result ) =>{
      resolve( result )
    })
    .catch( ( error ) => {
      reject( error )
    })
  })
}

export const getSubscriptionToMarketsList = () =>
{
  return new Promise( ( resolve, reject ) => {
    dbFindAll(watchListDB)
    .then( (results) => {
      resolve( results )
    })
    .catch( ( error ) => {
      reject( error )
    })
  })
}