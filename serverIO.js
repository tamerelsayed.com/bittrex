import express from 'express'
import path from 'path'
import http from 'http'
import SocketIO from 'socket.io'
import compression from 'compression'

import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import config from './webpack.config.js'

import { getMarketSummaries, startWebsocket } from './functions/bittrex'
import { getWatchlist, saveWatchlist, getSubscriptionToMarketsList } from './functions/mongoDB'

const DEFAULT_PORT = 3000;
const   app             = express(),
        DIST_DIR        = path.join( __dirname, 'public' ),
        HTML_FILE       = path.join( DIST_DIR, 'index.html' ),
        server          = http.Server(app),
        io              = new SocketIO(server),
        isDeveloping    = process.env.NODE_ENV !== 'production',
        port            = isDeveloping ? 3000 : process.env.PORT,
        sockets         = {},
        compiler        = webpack(config);

let MarketSummaries = {
    BTC: [],
    ETH: [],
    USDT: []
}

let MarketNames = {
    BTC: [],
    ETH: [],
    USDT: []
}

app.set("port", process.env.PORT || DEFAULT_PORT);
app.use( compression( {} ) );

if (isDeveloping) {
    app.use(webpackDevMiddleware(compiler, {
        publicPath: config.output.publicPath,
        contentBase: 'src',
        stats: {
            colors: true,
            hash: false,
            timings: true,
            chunks: false,
            chunkModules: false,
            modules: false
        }
    }));


    app.use(webpackHotMiddleware(compiler));

    app.get('*', function response(req, res) {
        res.sendFile(path.join(__dirname, 'public/index.html'));
    });
}
else
{
    app.use( express['static']( DIST_DIR ) );
    app.get('*', function response(req, res) {
        res.sendFile(path.join(__dirname, 'public/index.html'));
    });
}

let WatchListDataPerUser = {}

getSubscriptionToMarketsList()
.then( ( result ) =>
{
    result.forEach( ( data_for ) => {
        WatchListDataPerUser[data_for.user] = data_for
    })
})


io.on('connection', (socket) =>
{
    console.log('a user connected');
    const socketId = socket.id

    socket.on('UpdateWatchList',  ( from , data )  =>
    {
        console.log("UpdateWatchList >> from", from )
        console.log("UpdateWatchList >> data", data )
        saveWatchlist( [ data.query ], data.Mutation )
        .then( ( saveData ) => {
            io.emit('EMIT_watchList', 'IOserver', saveData.Currencies )
            io.emit('EMIT_alerts', 'IOserver', saveData.Currencies )

            WatchListDataPerUser[data.query.user].Currencies = saveData.Currencies
        })
        .catch ( ( error ) => {
            console.log('saveWatchlist  error >>', error )
        })
    });

    socket.on('GetWatchList',  ( from, data )  => {

        WatchListDataPerUser[ data.user ].socketID = socketId

        getWatchlist( [ data.query ] )
        .then( ( queryData ) => {

            io.emit('EMIT_watchList', 'IOserver', queryData[0] )
        })
        .catch ( ( error ) => {
            console.log('saveWatchlist  error >>', error )
        })
    });

    socket.on('updateExchangeState',  ( from, data )  => {

        const marketInfo = {}
        const MarketName = data.MarketName
        Object.keys( data ).map(function(key, index)
        {
            if ( marketInfo[ MarketName ] === undefined )
            {
                marketInfo[ MarketName ] = {}
            }
            marketInfo[ MarketName ][key] = data[key]
        })

        Object.keys( WatchListDataPerUser ).map(function(key, index)
        {
            if ( WatchListDataPerUser[key].Currencies.indexOf( MarketName ) !== -1 &&  WatchListDataPerUser[key].socketID !== undefined )
            {
                socket.broadcast.to(WatchListDataPerUser[key].socketID).emit('EMIT_updateExchangeState', 'IOserver', marketInfo[ MarketName ] )
            }
        })
    });

    socket.on('updateSummaryState',  ( from, data )  => {
        io.emit('EMIT_updateSummaryState', 'IOserver', data );
    });

    socket.on( 'GET_MarketNames',  ( from, data )  => {
        io.emit('EMIT_MarketNames', 'IOserver', MarketNames );
        io.emit('EMIT_MarketSummaries', 'IOserver', MarketSummaries );
    });

    socket.on( 'GET_MarketSummaries',  ( from, data )  => {
        getMarketSummaries( MarketSummaries, MarketNames )
        .then( ( data ) =>{
            MarketSummaries = data.MarketSummaries
            MarketNames = data.MarketNames
            io.emit('EMIT_MarketSummaries', 'IOserver', MarketSummaries );
            io.emit('EMIT_MarketNames', 'IOserver', MarketNames );
        })
    });

    socket.on('disconnect', () =>
    {
        Object.keys( WatchListDataPerUser ).map(function(key, index)
        {
            if ( WatchListDataPerUser[key].socketID === socketId )
            {
                delete WatchListDataPerUser[key].socketID
            }
        })
        console.log('user disconnected');
    });

    socket.on('ding', () => {
        io.emit('dong');
    });
});

server.listen(port, () => {
    console.log('[INFO] Listening on *:' + port);
    getMarketSummaries( MarketSummaries, MarketNames )
    .then( ( data ) =>{
        MarketSummaries = data.MarketSummaries
        MarketNames = data.MarketNames
        return ( startWebsocket( MarketNames.BTC ) )
    })
    .then( ( data ) => {
        console.log( "Websocet Date", data )
    })
});